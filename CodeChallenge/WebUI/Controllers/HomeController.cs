﻿using System.Web.Mvc;
using WebUI.Common.Helpers;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // load default values
            var calculatorModel = new BenefitsCalculatorModel
            {
                EmployeeSalary = AppConfigHelper.GetEmployeeSalary(),
                PayCheckPerYear = AppConfigHelper.GetPayCheckPerYear(),
                CostOfBenefitsEmployee = AppConfigHelper.GetCostOfBenefitsEmployee(),
                CostOfBenefitsDependent = AppConfigHelper.GetCostOfBenefitsDependent(),
                DiscountPct = AppConfigHelper.GetDiscountPct()
            };
            
            return View(calculatorModel);
        }

   
    }
}