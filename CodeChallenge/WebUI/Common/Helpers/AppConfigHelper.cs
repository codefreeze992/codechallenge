﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebUI.Common.Helpers
{
    public class AppConfigHelper
    {
        const string employeeSalary = "EmployeeSalary";
        const string payCheckPerYear = "PayCheckPerYear";
        const string costOfBenefitsEmployee = "CostOfBenefitsEmployee";
        const string costOfBenefitsDependent = "CostOfBenefitsDependent";
        const string discountPct = "DiscountPct";
        public static decimal GetEmployeeSalary()
        {
            if (ConfigurationManager.AppSettings[employeeSalary] == null)
                return 0;
            else
            {
                var requestAppSetting = ConfigurationManager.AppSettings[employeeSalary];
                if (requestAppSetting != null)
                {
                        decimal newid = 0;
                        bool success = decimal.TryParse(requestAppSetting, out newid);
                        if (success)
                            return newid;
                        else
                            return 0;
                }
                return (0);
            }
        }

        public static int GetPayCheckPerYear()
        {
            if (ConfigurationManager.AppSettings[payCheckPerYear] == null)
                return 0;
            else
            {
                var requestAppSetting = ConfigurationManager.AppSettings[payCheckPerYear];
                if (requestAppSetting != null)
                {
                    int newid = 0;
                    bool success = int.TryParse(requestAppSetting, out newid);
                    if (success)
                        return newid;
                    else
                        return 0;
                }
                return (0);
            }
        }
        public static decimal GetCostOfBenefitsEmployee()
        {
            if (ConfigurationManager.AppSettings[costOfBenefitsEmployee] == null)
                return 0;
            else
            {
                var requestAppSetting = ConfigurationManager.AppSettings[costOfBenefitsEmployee];
                if (requestAppSetting != null)
                {
                    decimal newid = 0;
                    bool success = decimal.TryParse(requestAppSetting, out newid);
                    if (success)
                        return newid;
                    else
                        return 0;
                }
                return (0);
            }
        }
        public static decimal GetCostOfBenefitsDependent()
        {
            if (ConfigurationManager.AppSettings[costOfBenefitsDependent] == null)
                return 0;
            else
            {
                var requestAppSetting = ConfigurationManager.AppSettings[costOfBenefitsDependent];
                if (requestAppSetting != null)
                {
                    decimal newid = 0;
                    bool success = decimal.TryParse(requestAppSetting, out newid);
                    if (success)
                        return newid;
                    else
                        return 0;
                }
                return (0);
            }
        }
        public static int GetDiscountPct()
        {
            if (ConfigurationManager.AppSettings[discountPct] == null)
                return 0;
            else
            {
                var requestAppSetting = ConfigurationManager.AppSettings[discountPct];
                if (requestAppSetting != null)
                {
                    int newid = 0;
                    bool success = int.TryParse(requestAppSetting, out newid);
                    if (success)
                        return newid;
                    else
                        return 0;
                }
                return (0);
            }
        }
    }
}