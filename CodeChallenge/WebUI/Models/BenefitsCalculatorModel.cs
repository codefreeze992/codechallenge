﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class BenefitsCalculatorModel
    {
        public decimal EmployeeSalary { get; set; }
        public int PayCheckPerYear { get; set; }
        public decimal CostOfBenefitsEmployee { get; set; }
        public decimal CostOfBenefitsDependent { get; set; }
        public int DiscountPct { get; set; }
    }
}